#ifndef __LOG_DATA_H
#define __LOG_DATA_H

#include "app_common.h"
#include "ff.h"

#define RTC_I2C_ADDRESS (0x68 << 1)
#define BCD2BIN(_val)	(_val - 6 * (_val >> 4))

typedef enum
{
  LOG_ERR = -1,
  LOG_OK = 0,
  LOG_ALREADY_OPEN = 1
} RetLog_e;

typedef struct
{
  uint16_t u16Sec;
  uint16_t u16Min;
  uint16_t u16Hour;
  uint16_t u16Day;
  uint16_t u16Month;
  uint16_t u16Year;
} TimeRTC_t;

typedef struct
{
  char sTimeStamp[20];		/* Log timestamp */
  char sData[13];			/* Measured value and property ID */
  uint8_t bSDCardMount;		/* 0 - SD card not mounted, 1 - SD card already mounted */
  uint32_t ui32LogPeriod;	/* Period of logging [ms] */
  uint8_t bLogEnabled;		/* 1 if log enabled, else 0 */
} LogData_t;

typedef struct
{
	uint16_t ui16NodeAdr;			/* Node address */
	uint32_t ui32AccMeas;			/* Accumulated measured data */
	uint8_t ui8CounterMeas;			/* Number of accumulated samples */
	uint32_t ui32AvgMeas;			/* Average of accumulated data */
	uint16_t ui16PropertyID;		/* Property ID of sensor descriptor */
	uint8_t bPrintMeasValueEnable;	/* Disable/enable print measured values to terminal */
	uint8_t bLogMeasValueEnable;	/* Disable/enable log measured data */
} MeasData_t;


extern LogData_t g_sDataLog;
extern MeasData_t g_sSensorData[MAX_NODES];


FRESULT LogEnableRequet(uint32_t LogPeriod);		/* Log period in [sec] */
FRESULT LogDisableRequet(void);
void AppliMeshLogDataTask(void);

void TriggerRTC_I2C(void);

#endif /* __LOG_DATA_H */
