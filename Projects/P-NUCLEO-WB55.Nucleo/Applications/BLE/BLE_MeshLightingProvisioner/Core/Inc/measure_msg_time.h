/********************* Measure message propagation time **************/

#ifndef __MEASURE_MSG_TIME_H
#define __MEASURE_MSG_TIME_H


/* Includes ------------------------------------------------------------------*/
#include "app_common.h"
#include "types.h"

typedef struct
{
  uint32_t ui32Counter;
  uint32_t ui32CaptureStart;
  uint32_t ui32CaptureEnd;
  uint16_t dstAdr;      			/* Address where will be 'measure' message send */
  uint16_t ui16CounterTxMsg;		/* Count transmit messages */
  uint16_t ui16CounterRxMsg;		/* Count receive messages */
  uint8_t bClearReq;
} TimeMeas_t;


extern TimeMeas_t g_sMsgTimePropagation;

void Appli_SyncMeasTimer(void);               										/* Synchronize timer for measureme message time propagation */
void Appli_StartMeasureMsgTime(MOBLE_ADDRESS dstAdr, uint8_t publishPeriod_100ms);   /* Run measurement message time propagation */
void Appli_StopMeasureMsgTime(MOBLE_ADDRESS dstAdr);								 /* Stop measurement message time propagation */

void Trigger_SendSensorGet(void);

#endif /* __MEASURE_MSG_TIME_H */
