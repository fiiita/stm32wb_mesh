#include "measure_msg_time.h"
#include "ble_mesh.h"
#include "mesh_cfg.h"
#include "common.h"
#include "lp_timer.h"
#include "appli_config_client.h"


TimeMeas_t g_sMsgTimePropagation;


/* TIM2 overflow interrupt - period 100us*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	g_sMsgTimePropagation.ui32Counter++;
}

/* Synchronize timer for measureme message time propagation */
void Appli_SyncMeasTimer(void)
{
	/* Trigger sync timer */
	HAL_GPIO_WritePin(OUTPUT_SYNC_PORT, OUTPUT_SYNC_PIN, GPIO_PIN_SET);
	g_sMsgTimePropagation.ui32Counter = 0;
	HAL_GPIO_WritePin(OUTPUT_SYNC_PORT, OUTPUT_SYNC_PIN, GPIO_PIN_RESET);

}

/* Run measurement message time propagation
 * Concentrator -----Sensor Server publication set ---------> Remote node (address = dstAdr)
 * Remote node reads timer value (tStart) and and write it to the Count24 characteristic.
 * Remote node also append to payload uint16 TxMsg value, which is incremented when publishing.
 * Concentrator <----Sensor Status------ Remote node
 * Concentrator reads timer value (tEnd).
 * tDelta[ms]= (tEnd - tStart) / 10
 *
 * Publish period of remote node is set as 100ms*publishPeriod_100ms, e.g. publishPeriod_100ms = 10 -> publish  period = 1 sec
 * */
void Appli_StartMeasureMsgTime(MOBLE_ADDRESS dstAdr, uint8_t publishPeriod_100ms)
{
	MOBLE_RESULT res;

	/* Prepare for measure */
	g_sMsgTimePropagation.ui32CaptureStart = 0;
	g_sMsgTimePropagation.ui32CaptureEnd = 0;
	g_sMsgTimePropagation.dstAdr = dstAdr;			/* Store address where will be message send */
	g_sMsgTimePropagation.ui16CounterTxMsg = 0;
	g_sMsgTimePropagation.ui16CounterRxMsg = 0;

	res = AppliConfigClient_PublicationSet(dstAdr, 0xc100, (MOBLEUINT32)(0x1100), publishPeriod_100ms);
}

/* Stop measure message time propagation */
void Appli_StopMeasureMsgTime(MOBLE_ADDRESS dstAdr)
{
	uint8_t i;
	for(i = 0; i < 3; i++)
		AppliConfigClient_PublicationSet(dstAdr, 0xc100, (MOBLEUINT32)(0x1100), 0);
}
