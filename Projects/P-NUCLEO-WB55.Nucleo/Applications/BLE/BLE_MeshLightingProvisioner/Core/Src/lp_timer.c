/**
 ***************************************************************************************
  * File Name          : lp_timer.c
  * Description        : Low power timer to be used within Mesh Application.
 ***************************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the 
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/  
#include "app_common.h"

#include "lp_timer.h"
#include "log_data.h"
#include "measure_msg_time.h"
#include "appli_config_client.h"

/* Exported function prototypes --------------------------------------------------------*/
extern void LpTimerUnpvNode_Cb(void);
extern void AppliConfigClient_LPT(void);

/* Private function prototypes -----------------------------------------------*/
static void LpTimerCb(void);

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
  uint32_t LpTimeLeftOnEntry;
  uint8_t LpTimer_Id;
} LpTimerContext_t;

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static LpTimerContext_t LpTimerContext[3];

/* Functions Definition ------------------------------------------------------*/

/**
 * @brief Initialize the low power timer
 *
 * @param  None
 * @retval None
 */
void LpTimerInit(void)
{
  (void) HW_TS_Create(CFG_TIM_PROC_ID_ISR, &(LpTimerContext[LPTIMER_LOG].LpTimer_Id), hw_ts_Repeated , LpTimerCb);
  (void) HW_TS_Create(CFG_TIM_PROC_ID_UNPV_NODE, &(LpTimerContext[LPTIMER_UNPV_NODE].LpTimer_Id), hw_ts_SingleShot , LpTimerUnpvNode_Cb);
  (void) HW_TS_Create(CFG_TIM_PROC_ID_CONFIG_CLI, &(LpTimerContext[LPTIMER_CONFIG_CLI].LpTimer_Id), hw_ts_SingleShot , AppliConfigClient_LPT);
  return;
}

/**
 * @brief  Request to start a low power timer ( running is stop mode )
 *
 * @param  time_to_sleep : in ms
 * @retval None
 */
void LpTimerStart(uint32_t time_to_sleep, uint16_t LpNum)
{
  /* Converts the number of ms into hw timer tick */
  if(time_to_sleep > 0x400000)
  {
    time_to_sleep = time_to_sleep / (CFG_TS_TICK_VAL);
    time_to_sleep *= 1000;
  }
  else
  {
    time_to_sleep *= 1000;
    time_to_sleep = time_to_sleep / (CFG_TS_TICK_VAL);
  }

  HW_TS_Start(LpTimerContext[LpNum].LpTimer_Id, time_to_sleep);

  /**
   * There might be other timers already running in the timer server that may elapse
   * before this one.
   * Store how long before the next event so that on wakeup, it will be possible to calculate
   * how long the tick has been suppressed
   */
  LpTimerContext[LpNum].LpTimeLeftOnEntry = HW_TS_RTC_ReadLeftTicksToCount();

  return;
}

void LpTimerStop(uint16_t LpNum)
{
	HW_TS_Stop(LpTimerContext[LpNum].LpTimer_Id);
	return;
}

/**
 * @brief  Read how long the timer has run
 *
 * @param  None
 * @retval The time elapsed in ms
 */
uint32_t LpGetElapsedTime(uint16_t LpNum)
{
  uint32_t return_value;

  return_value = (CFG_TS_TICK_VAL) * (uint32_t)(LpTimerContext[LpNum].LpTimeLeftOnEntry - HW_TS_RTC_ReadLeftTicksToCount());
  return_value = return_value / 1000;

  /**
   * The system may have been out from another reason than the timer
   * Stop the timer after the elapsed time is calculated other wise, HW_TS_RTC_ReadLeftTicksToCount()
   * may return 0xFFFF ( TIMER LIST EMPTY )
   * It does not hurt stopping a timer that exists but is not running.
   */
  HW_TS_Stop(LpTimerContext[LpNum].LpTimer_Id);

  return return_value;
}


/*************************************************************
 *
 * LOCAL FUNCTIONS
 *
 *************************************************************/
/**
 * @brief Low power timer callback
 *
 * @param  None
 * @retval None
 */
static void LpTimerCb( void )
{
  /**
   * Nothing to be done
   */
	//BSP_LED_Toggle(LED_RED);
	TriggerRTC_I2C();

  return;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

