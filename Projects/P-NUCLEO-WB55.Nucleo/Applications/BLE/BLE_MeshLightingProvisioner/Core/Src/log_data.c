#include "log_data.h"
#include "app_fatfs.h"
#include "mesh_cfg.h"
#include "lp_timer.h"
#include "stm32_seq.h"
#include "appli_nvm.h"

static void ConvRxData2Time(void);

extern I2C_HandleTypeDef hi2c1;
TimeRTC_t g_sTime;
uint8_t aTx = 0x00;
uint8_t aRx[7];

LogData_t g_sDataLog;

/* USER - measured data */
MeasData_t g_sSensorData[MAX_NODES];

FRESULT LogEnableRequet(uint32_t LogPeriod)
{
	FRESULT ret;
	uint32_t iLogPeriod;
	uint16_t i;
	MOBLEINT16 subPageIdx;
	MOBLEUINT32 valid;
	MOBLEUINT32 *subPageOffset;
	MOBLEUINT32 addressValidPage;

	iLogPeriod = (LogPeriod > 0) ? LogPeriod : 1;		/* Minimum period 1 sec */
	iLogPeriod = 1000 * iLogPeriod;						/* Conv to ms */

	if(g_sDataLog.bLogEnabled == 0)
	{
		ret = ConnectSDCard();

		if(ret != FR_OK)
		{
			/* Error, check if SD card is inserted */
			return ret;
		}

		for(i = 0; i < MAX_NODES; i++)
		{
			g_sSensorData[i].ui32AccMeas = 0;
			g_sSensorData[i].ui8CounterMeas = 0;
		}

		LpTimerStart(iLogPeriod, LPTIMER_LOG);
		g_sDataLog.ui32LogPeriod = iLogPeriod;
		g_sDataLog.bLogEnabled = 1;

		/* Enable log only for provisioned nodes */
		AppliNvm_FindFirstEmptyPage(&subPageIdx, PRVN_NVM_CHUNK_SIZE,
		                                      PRVN_NVM_MAX_SUBPAGE-1,PRVN_NVM_BASE_OFFSET);

		for(i = 1; i <= subPageIdx; i++)
		{
			addressValidPage = PRVN_NVM_BASE_OFFSET + PRVN_NVM_CHUNK_SIZE*i - 4;	/* Read last 4 bytes of each node */
			subPageOffset = &addressValidPage;
			/* Read 8 bytes and check if it's Empty */
			memcpy((void*)&valid, (void*)(*subPageOffset), 4);
			if ((valid == 0x11111111))
				g_sSensorData[i].bLogMeasValueEnable = 1;
			else
				g_sSensorData[i].bLogMeasValueEnable = 0;
		}

	}
	else
	{
		ret = FR_EXIST;
		return ret;
	}

	return ret;
}

FRESULT LogDisableRequet(void)
{
	FRESULT ret;

	if(g_sDataLog.bLogEnabled == 1)
	{
		LpTimerStop(LPTIMER_LOG);		/* Stop timer which triggers I2C read from RTC */

		ret = CloseFileSDCard(0);		/* Close all files */
		if(ret < 0)
		{
			/* Error during closing files */
			TRACE_I(TF_SERIAL_PRINTS,"Closing files failed!");
		}

		ret = DisconnectSDCard();
		if(ret != FR_OK)
		{
			/* Error during unmounting SD card */
			TRACE_I(TF_SERIAL_PRINTS,"Disconnecting SD card failed!");
			return ret;
		}

		g_sDataLog.ui32LogPeriod = 0;
		g_sDataLog.bLogEnabled = 0;
	}
	else
		ret = FR_EXIST;
	return ret;
}


/******************Log data to SD card - related to CFG_TASK_MESH_LOG_DATA_REQ_ID  *********/
void AppliMeshLogDataTask(void)
{
	uint16_t i;
	int32_t i32Res;

	ConvRxData2Time();

	if(g_sDataLog.bLogEnabled == 1)
	{
		TRACE_I(TF_SERIAL_PRINTS,"Log Node: ");

		for(i = 0; i < MAX_NODES; i++)
		{
//			if(g_sSensorData[i].ui8CounterMeas > 0)
//			{
			if(g_sSensorData[i].bLogMeasValueEnable == 1)
			{
				/* Log data for node 0xi */
				if(g_sSensorData[i].ui8CounterMeas != 0)
					g_sSensorData[i].ui32AvgMeas = (uint32_t)((float)g_sSensorData[i].ui32AccMeas / (float)g_sSensorData[i].ui8CounterMeas);
				else
					g_sSensorData[i].ui32AvgMeas = 0;

				g_sSensorData[i].ui8CounterMeas = 0;
				g_sSensorData[i].ui32AccMeas = 0;
				sprintf((char*)g_sDataLog.sData, "0x%04x %05lu\n", g_sSensorData[i].ui16PropertyID, g_sSensorData[i].ui32AvgMeas);

				i32Res = LogToSDCard(i);
				TRACE_I(TF_SERIAL_PRINTS,"%u, ",i);
			}
		}
	}

	TRACE_I(TF_SERIAL_PRINTS,"Log complete\r\n");

	LpTimerStart(g_sDataLog.ui32LogPeriod, LPTIMER_LOG);		/* Next log after 10sec */
	return;
}


/************************** I2C control ********************************/
/* Tx Transfer completed callback */
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{
  if(HAL_I2C_Master_Receive_IT(&hi2c1, RTC_I2C_ADDRESS, &aRx[0], 7) != HAL_OK)
  {
	  TRACE_I(TF_SERIAL_PRINTS,"I2C: Read from RTC failed!");
  }
}

/* Rx Transfer completed callback */
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{
  UTIL_SEQ_SetTask( 1<<CFG_TASK_MESH_LOG_DATA_REQ_ID, CFG_SCH_PRIO_0);
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *I2cHandle)
{
  /** Error_Handler() function is called when error occurs.
    * 1- When Slave doesn't acknowledge its address, Master restarts communication.
    * 2- When Master doesn't acknowledge the last data transferred, Slave doesn't care in this example.
    */
  if (HAL_I2C_GetError(I2cHandle) != HAL_I2C_ERROR_AF)
  {
	  TRACE_I(TF_SERIAL_PRINTS,"I2C: ErrorCb!");
  }
}

void TriggerRTC_I2C(void)
{
	if(HAL_I2C_Master_Transmit_IT(&hi2c1, RTC_I2C_ADDRESS, &aTx, 1)!= HAL_OK)
	{
		TRACE_I(TF_SERIAL_PRINTS,"I2C: Set pointer in RTC module failed!");
	}
}


static void ConvRxData2Time(void)
{
	g_sTime.u16Sec = BCD2BIN((aRx[0] & 0x7f));
	g_sTime.u16Min = BCD2BIN(aRx[1]);
	g_sTime.u16Hour = BCD2BIN(aRx[2]);
	g_sTime.u16Day = BCD2BIN(aRx[4]);
	g_sTime.u16Month = BCD2BIN(aRx[5]);
	g_sTime.u16Year = BCD2BIN(aRx[6]) + 2000;
	sprintf((char*)g_sDataLog.sTimeStamp, "%u/%02u/%02u %02u:%02u:%02u ",\
			g_sTime.u16Year, g_sTime.u16Month, g_sTime.u16Day, g_sTime.u16Hour, g_sTime.u16Min, g_sTime.u16Sec);
}
