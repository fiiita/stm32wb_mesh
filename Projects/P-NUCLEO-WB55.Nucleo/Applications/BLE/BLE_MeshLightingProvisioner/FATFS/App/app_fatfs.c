/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    FatFs/FatFs_uSD_Standalone/FatFs/App/app_fatfs.c
  * @author  MCD Application Team
  * @brief   FatFs_uSD_Standalone application file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "app_fatfs.h"
#include "main.h"

#include "log_data.h"
#include <stdio.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum {
  APPLICATION_IDLE = 0,
  APPLICATION_INIT,
  APPLICATION_RUNNING,
}FS_FileOperationsTypeDef;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define FATFS_MKFS_ALLOWED 0
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
FATFS SDFatFs;    /* File system object for SD logical drive */
FIL SDFile;       /* File  object for SD */
char SDPath[4];   /* SD logical drive path */
/* USER CODE BEGIN PV */
FS_FileOperationsTypeDef Appli_state = APPLICATION_IDLE;
uint8_t workBuffer[_MAX_SS];

extern LogData_t g_sDataLog;
HandleFiles_t g_sFileHandler[MAX_NODES];	/* Logging data from 5 nodes */

/* USER CODE END PV */



/**
  * @brief  FatFs initialization
  * @param  None
  * @retval Initialization result
  */
int32_t MX_FATFS_Init(void)
{
  /*## FatFS: Link the disk I/O driver(s)  ###########################*/
  if (FATFS_LinkDriver(&SD_Driver, SDPath) != 0)
  /* USER CODE BEGIN FATFS_Init */
  {
    return APP_ERROR;
  }
  else
  {
    Appli_state = APPLICATION_INIT;
    return APP_OK;
  }
  /* USER CODE END FATFS_Init */
}

/**
  * @brief  FatFs deinitialization
  * @param  None
  * @retval Deinitialization result
  */
int32_t MX_FATFS_Deinit(void)
{
	FRESULT res;
  /*## FatFS: Unlink the disk I/O driver(s)  ###########################*/
	res = FATFS_UnLinkDriverEx(SDPath, 0);
	if(res == FR_OK)
		Appli_state = APPLICATION_IDLE;
	return res;
}

/**
  * @brief  FatFs application main process
  * @param  None
  * @retval Process result
  */
int32_t MX_FATFS_Process(void)
{
  /* USER CODE BEGIN FATFS_Process */
  int32_t process_res = APP_OK;
  /* Mass Storage Application State Machine */
  switch(Appli_state)
  {
  case APPLICATION_INIT:
    if(BSP_SD_GetCardState() == BSP_SD_OK)
    {
#if FATFS_MKFS_ALLOWED
      FRESULT res;

      res = f_mkfs(SDPath, FM_ANY, 0, workBuffer, sizeof(workBuffer));

      if (res != FR_OK)
      {
        process_res = APP_ERROR;
      }
      else
      {
        process_res = APP_INIT;
        Appli_state = APPLICATION_RUNNING;
      }
#else
      process_res = APP_INIT;
      Appli_state = APPLICATION_RUNNING;
#endif
    }
    else
    {
    process_res = APP_ERROR;

    }

    break;
  case APPLICATION_RUNNING:

      Appli_state = APPLICATION_IDLE;
    break;

  case APPLICATION_IDLE:
  default:
    break;
  }
  return process_res;
  /* USER CODE END FATFS_Process */
}

/**
  * @brief  Gets Time from RTC (generated when FS_NORTC==0; see ff.c)
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
  /* USER CODE BEGIN get_fattime */
  return 0;
  /* USER CODE END get_fattime */
}

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN Application */


/* Logging data from g_sDataLog structure to SD card */
int32_t LogToSDCard(uint16_t ui16Address)
{
	FRESULT res; /* FatFs function common result code */
	uint32_t byteswritten; /* File write counts */

	if(Appli_state == APPLICATION_IDLE)
	{
	  res = ConnectSDCard();
	  if(res == APP_ERROR)
	  {
		  BSP_LED_On(LED_RED);		/* Error */
		  return APP_ERROR;
	  }
	}

	/* Open closed file */
	if(g_sFileHandler[ui16Address].bOpened == 0)
	{
		res = OpenFileSDCard(ui16Address);
		if(res != FR_OK)
			return res;
	}

	/* Write data to the text file */
	res = f_write(&g_sFileHandler[ui16Address].SDFile, g_sDataLog.sTimeStamp, 20, (void *)&byteswritten);
	res = f_write(&g_sFileHandler[ui16Address].SDFile, g_sDataLog.sData, sizeof(g_sDataLog.sData), (void *)&byteswritten);

	if((byteswritten > 0) && (res == FR_OK))
	{
		res = f_sync(&g_sFileHandler[ui16Address].SDFile);
		return res;
	}

	/* Error */
	return APP_ERROR;
}


int32_t ConnectSDCard(void)
{
	FRESULT res;

	res = MX_FATFS_Init();		/* if success then Appli_state = APPLICATION_INIT */
	if(res == APP_ERROR)
	{
		BSP_LED_On(LED_RED);		/* Error */
	}

	if(g_sDataLog.bSDCardMount == 0)
	{
	  /* Register the file system object to the FatFs module */
	  res = f_mount(&SDFatFs, (TCHAR const*)SDPath, 1);
	  if(res == FR_OK)
	  {
		  g_sDataLog.bSDCardMount = 1;
	  }
	  else
		  MX_FATFS_Deinit();		/* Unlink driver */
	}

	return res;
}

int32_t DisconnectSDCard(void)
{
	FRESULT res;

    /* Unregister the file system object to the FatFs module */
    res = f_mount(NULL, (TCHAR const*)SDPath, 1);
	if(res== FR_OK)
	{
	  g_sDataLog.bSDCardMount = 0;
	}
	res = MX_FATFS_Deinit();		/* if success then  Appli_state = APPLICATION_IDLE */

	return res;
}

int32_t OpenFileSDCard(uint16_t ui16Address)
{
	FRESULT res;

	if(g_sFileHandler[ui16Address].bOpened)
	{
		/* File already opened */
		res = 1;
	}
	else
	{
	    char sFileName[8];
	    sprintf(sFileName, "%04X.txt", ui16Address);

	    res = f_open(&g_sFileHandler[ui16Address].SDFile, sFileName, FA_OPEN_APPEND  | FA_WRITE);
		if(res == FR_OK)
		{
			g_sFileHandler[ui16Address].bOpened = 1;
			g_sFileHandler[ui16Address].ui16NodeAddress = ui16Address;
		}
		else
			res = -1;
	}

	return res;
}

int32_t CloseFileSDCard(uint16_t ui16Address)
{
	FRESULT res;
	uint16_t nd;

	/* Close all files */
	if(ui16Address == 0)
	{
		for(nd = 1; nd < MAX_NODES; nd++)
		{
			if(CloseFileSDCard(nd) < 0)
				res |= -1;
		}
		return res;
	}

	if(!g_sFileHandler[ui16Address].bOpened)
	{
		/* File already closed */
		res = 1;
	}
	else
	{
		res = f_close(&g_sFileHandler[ui16Address].SDFile);
		if(res == FR_OK)
		{
			g_sFileHandler[ui16Address].bOpened = 0;
			g_sFileHandler[ui16Address].ui16NodeAddress = 0;
		}
	}
	return res;
}


/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
