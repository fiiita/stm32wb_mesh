/**
  ******************************************************************************
  * @file    appli_sensor.c
  * @author  BLE Mesh Team
  * @brief   Application interface for Sensor Mesh Models 
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "hal_common.h"
#include "types.h"
#include "sensor_cfg_usr.h"
#include "sensors.h"
#include "appli_sensor.h"
#include "appli_light_lc.h"
#include "mesh_cfg.h"
#include "string.h"
#include "common.h"

#include "log_data.h"

#ifdef MEASURE_MSG_TIME_PROPAGATION
#include "measure_msg_time.h"
#endif

#define MATLAB_PRINT		0


/** @addtogroup ST_BLE_Mesh
 *  @{
 */

/** @addtogroup Application_Mesh_Models
 *  @{
 */

MOBLE_RESULT Appli_Sensor_ValueSet(MOBLEUINT8 sensorOffset, 
                                   MOBLEUINT32 value);
static uint16_t ExtractPropertyID(const uint8_t *pStatus, uint8_t *pLength);	/* Extract property ID and lenght ... MshMDLv1.0.1 4.2.14 */

/* Private variables ---------------------------------------------------------*/

/* ALIGN(4) */
__attribute__((aligned(4)))const sensor_server_init_params_t SensorServerInitParams = SENSOR_SERVER_INIT_PARAMS;

/* ALIGN(4) */
__attribute__((aligned(4)))const sensor_server_cb_t AppliSensor_cb = 
{
  /* Sensor Model callbacks */
  Appli_Sensor_CadenceGet,
  Appli_Sensor_CadenceSet,
  Appli_Sensor_CadenceSetUnack,
  Appli_Sensor_SettingsGet,
  Appli_Sensor_SettingGet,
  Appli_Sensor_SettingSet,
  Appli_Sensor_SettingSetUnack,
  Appli_Sensor_DescriptorGet,
  Appli_Sensor_Get,
  Appli_Sensor_ColumnGet,
  Appli_Sensor_SeriesGet,
  Appli_Sensor_ReadDescriptor,
  Appli_Sensor_ReadValue,
  Appli_Sensor_ReadColumn,
  Appli_Sensor_ReadSeries,
  Appli_Sensor_IsFastCadence,
  Appli_Sensor_IsStatusTrigger,
  Appli_Sensor_Descriptor_Status,
  Appli_Sensor_Cadence_Status,
  Appli_Sensor_Settings_Status,
  Appli_Sensor_Setting_Status,
  Appli_Sensor_Status,
  Appli_Sensor_Column_Status,
  Appli_Sensor_Series_Status  
};

#if 0
/**
  * @brief Pressure sensor init
  */
__attribute__((aligned(4)))const PRESSURE_InitTypeDef Lps25InitParams =
{
  LPS25HB_ODR_1Hz,
  LPS25HB_BDU_READ, 
  LPS25HB_DIFF_ENABLE,  
  LPS25HB_SPI_SIM_3W,  
  LPS25HB_P_RES_AVG_32,
  LPS25HB_T_RES_AVG_16 
};
#endif

/**
  * @brief Variables for people 
  */
MOBLEUINT8 AppliSensorReadFromSensor = 0; /* Used for PTS testing */
MOBLEUINT8 PresentTemperatureValue = 0;
MOBLEUINT8 PreviousTemperatureValue = 0;
MOBLEUINT8 Occupancy_Flag = MOBLE_FALSE;
extern MOBLEUINT8 NumberOfElements;
extern MOBLEUINT8 ProvisionFlag;
MOBLEUINT8 Sensor_Setting_Access  = 0x01 ;
MOBLEUINT32 PresentPeopleCount = 0;
MOBLEUINT32 PreviousPeopleCount = 0;

extern Sensor_Status_Publish_t g_sSensorStatusData;
extern RTC_HandleTypeDef hrtc;
//extern MeasData_t g_sSensorData[MAX_NODES];

/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
#ifdef ENABLE_SENSOR_MODEL_SERVER
/******************************************************************************/


/**
  * @brief  Callback corresponding to Sensor_CadenceGet_cb
  * @param  Cadence parameters
* @retval None
*/ 
void Appli_Sensor_CadenceGet(sensor_CadenceCbParam_t* pCadenceParam,
                             MOBLEUINT32 length,
                             MOBLE_ADDRESS peerAddr,
                             MOBLE_ADDRESS dstPeer,
                             MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X  element index %X  peer addr %X dst peer %X\r\n",
                      pCadenceParam->property_ID, elementIndex, peerAddr, dstPeer);
}
  

/**
  * @brief  Callback corresponding to Sensor_CadenceSet_cb
  * @param  Cadence parameters
* @retval None
*/ 
void Appli_Sensor_CadenceSet(sensor_CadenceCbParam_t* pCadenceParam,
                             MOBLEUINT32 length,
                             MOBLE_ADDRESS peerAddr,
                             MOBLE_ADDRESS dstPeer,
                             MOBLEUINT8 elementIndex)                                    
{
  TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X dst peer %X\r\n",
                      pCadenceParam->property_ID, elementIndex, peerAddr, dstPeer); 
}


/**
  * @brief  Callback corresponding to Sensor_CadenceSetUnack_cb
  * @param  Cadence parameters
* @retval None
*/ 
void Appli_Sensor_CadenceSetUnack(sensor_CadenceCbParam_t* pCadenceParam,
                                  MOBLEUINT32 length,
                                  MOBLE_ADDRESS peerAddr,
                                  MOBLE_ADDRESS dstPeer,
                                  MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X dst peer %X\r\n",
                      pCadenceParam->property_ID, elementIndex, peerAddr, dstPeer);
}


/**
  * @brief  Callback corresponding to Sensor_SettingsGet_cb
  * @param  Setting parameters
* @retval None
*/ 
void Appli_Sensor_SettingsGet(sensor_SettingsCbParams_t* pSettingParam,
                              MOBLEUINT32 length,
                              MOBLE_ADDRESS peerAddr,
                              MOBLE_ADDRESS dstPeer,
                              MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X dst peer %X\r\n",
                      pSettingParam->propertyId, elementIndex, peerAddr, dstPeer); 
}
                                     
/**
  * @brief  Callback corresponding to Sensor_SettingGet_cb
  * @param  Setting parameters
* @retval None
      */
void Appli_Sensor_SettingGet(sensor_SettingCbParams_t* pSettingParam,
                             MOBLEUINT32 length,
                             MOBLE_ADDRESS peerAddr,
                             MOBLE_ADDRESS dstPeer,
                             MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X dst peer %X\r\n",
                      pSettingParam->property_ID, elementIndex, peerAddr, dstPeer);
}
      
      
/**
  * @brief  Callback corresponding to Sensor_SettingSet_cb
  * @param  Setting parameters
* @retval None
*/ 
void Appli_Sensor_SettingSet(sensor_SettingCbParams_t* pSettingParam,
                             MOBLEUINT32 length,
                             MOBLE_ADDRESS peerAddr,
                             MOBLE_ADDRESS dstPeer,
                             MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X dst peer %X\r\n",
                      pSettingParam->property_ID, elementIndex, peerAddr, dstPeer);
}  
        
      
/**
  * @brief  Callback corresponding to Sensor_SettingSetUnack_cb
  * @param  Setting parameters
* @retval None
*/
void Appli_Sensor_SettingSetUnack(sensor_SettingCbParams_t* pSettingParam,
                                  MOBLEUINT32 length,
                                  MOBLE_ADDRESS peerAddr,
                                  MOBLE_ADDRESS dstPeer,
                                  MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X dst peer %X\r\n",
                      pSettingParam->property_ID, elementIndex, peerAddr, dstPeer);
}

                                    
/**
  * @brief  Callback corresponding to Sensor_DescriptorGet_cb
  * @param  Descriptor parameters
* @retval None
*/ 
void Appli_Sensor_DescriptorGet(MOBLEUINT8 prop_ID,
                                MOBLEUINT32 length,
                                MOBLE_ADDRESS peerAddr,
                                MOBLE_ADDRESS dstPeer,
                                MOBLEUINT8 elementIndex)
{
  if(length == 0)
  {
    TRACE_M(TF_SENSOR, "Sensor Descriptor data for all sensors on element index %X peer addr %X, dst peer %X\r\n",
                        elementIndex, peerAddr, dstPeer);
  }
  else
  {
    TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X, dst peer %X\r\n",
                        prop_ID, elementIndex, peerAddr, dstPeer);
  }
}
      
      
/**
  * @brief  Callback corresponding to Sensor_Get_cb
  * @param  Get parameters
* @retval None
*/ 
void Appli_Sensor_Get(MOBLEUINT16 prop_ID,
                      MOBLEUINT32 length,
                      MOBLE_ADDRESS peerAddr,
                      MOBLE_ADDRESS dstPeer,
                      MOBLEUINT8 elementIndex)
{
  if(length == 0)
  {
    TRACE_M(TF_SENSOR, "Sensor Data for all sensors on element index %X peer addr %X dst peer %X\r\n",
                       elementIndex, peerAddr, dstPeer);
  }
  else
  {
    TRACE_M(TF_SENSOR, "Property ID %X element index %X peer addr %X dst peer %X \r\n",
                      prop_ID, elementIndex, peerAddr, dstPeer); 
  }
}
      
  
/**
  * @brief  Callback corresponding to Sensor_ColumnGet_cb
  * @param  Column parameters
* @retval None
*/
void Appli_Sensor_ColumnGet(sensor_ColumnCbParams_t* pColumnParam,
                            MOBLEUINT32 length,
                            MOBLE_ADDRESS peerAddr,
                            MOBLE_ADDRESS dstPeer,
                            MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X  element index %X peer addr %X dst peer %X\r\n",
                      pColumnParam->propertyId, elementIndex, peerAddr, dstPeer);
}


/**
  * @brief  Callback corresponding to Sensor_SeriesGet_cb
  * @param  Series parameters
* @retval None
*/ 
void Appli_Sensor_SeriesGet(sensor_SeriesCbParams_t* pSeriesParam,
                            MOBLEUINT32 length,
                            MOBLE_ADDRESS peerAddr,
                            MOBLE_ADDRESS dstPeer,
                            MOBLEUINT8 elementIndex)
{
  TRACE_M(TF_SENSOR, "Property ID %X  Raw Value X1 %d  Raw Value X2 %d\r\n",
                      pSeriesParam->propertyId,
                      pSeriesParam->rawValueX1,
                      pSeriesParam->rawValueX2);  
}


/**
  * @brief  Callback corresponding to Sensor_ReadDescriptor_cb
  *         Update SENSORX descriptor identified by sensorOffset
  *         Read from application to optimize RAM
  * @param  Sensor offset in sensor init structure
  * @param  descriptor parameters
  * @retval Fail if sensor doesn't exist or property ID mismatch occurrs
  *         else Success
  */
MOBLE_RESULT Appli_Sensor_ReadDescriptor(MOBLEUINT8 sensorOffset,
                                         sensor_DescriptorCbParams_t* pDescriptorParams)
{
  MOBLE_RESULT result = MOBLE_RESULT_SUCCESS;
  const sensor_init_params_t* pSensorInitParams = NULL;
  
  if (sensorOffset < SensorServerInitParams.sensorsCount)
  {
    pSensorInitParams = &(SensorServerInitParams.sensorInitParams[sensorOffset]);
    
    if (pSensorInitParams->propertyId != pDescriptorParams->propertyId)
    {
      result = MOBLE_RESULT_FAIL;
    }
    else
    {
      /* */
    }
  }
  else
  {
    result = MOBLE_RESULT_FAIL;
  }
  
  if (pSensorInitParams != NULL &&
      pDescriptorParams != NULL)
  {
    pDescriptorParams->positiveTolerance = pSensorInitParams->positiveTolerance;
    pDescriptorParams->negativeTolerance = pSensorInitParams->negativeTolerance;
    pDescriptorParams->samplingFunction = pSensorInitParams->samplingFunction;
    pDescriptorParams->measurementPeriod = pSensorInitParams->measurementPeriod;
    pDescriptorParams->updateInterval = pSensorInitParams->updateInterval;
  }
  else
  {
    result = MOBLE_RESULT_FAIL;
  }

  return result;
}


/**
  * @brief  Callback corresponding to Sensor_ReadValue_cb
  *         Read sensor value and update buffer
  *         data length <= 128
  *         PreviousTemperatureValue, PresentTemperatureValue  to be updated 
  *         everytime sensor value is changed and call to Sensor_UpdateCadence
  * @param  Sensor offset in sensor init structure
  * @param  Value parameters
  * @retval Fail if sensor doesn't exist
  *         else Success
  */
MOBLE_RESULT Appli_Sensor_ReadValue(MOBLEUINT8 sensorOffset,
                                    sensor_ValueCbParams_t* pValueParams)
{
  MOBLE_RESULT result = MOBLE_RESULT_SUCCESS;
//  PRESSURE_StatusTypeDef tempStatus = PRESSURE_OK;
  MOBLEINT16 temp = 0;
  MOBLEINT8 temperature8 = 0;
  float pressure = 0;
  
  /* sensor offset exist */
  if (sensorOffset < SensorServerInitParams.sensorsCount)
  {
    if (sensorOffset == 0) /* Present Ambient Temperature */
    {
      if(AppliSensorReadFromSensor == 0) /* Normal mode */
      {
        /* Temperature, Temperature8 format, M=1, d=0, b=-1 */
//        tempStatus = LPS25HB_I2C_ReadRawTemperature(&temp);
//        if (tempStatus == PRESSURE_OK)
        {
          TRACE_M(TF_SENSOR, "Temperature sensor raw value %d\r\n" , temp);
        
          /* Convert temperature raw value to Temperature8 format */
          temp = (temp/240) + 85;
        
          if (temp < -64*2)
          {
            temp = -64*2;
          }
          else if (temp > 63.5*2)
          {
            temp = 63.5*2;
          }
        
          temperature8 = temp;
        
          pValueParams->data[0] = (MOBLEUINT8)temperature8;

          if (pValueParams->data[0] == 0xFF)
          {
            /* 0xFF is unknown but here it is -1
            -1 is approximated to 0 */
            pValueParams->data[0] = 0x00;
          }
        
          TRACE_M(TF_SENSOR, "Temperature8 raw value %d, actual value %f\r\n", 
                              temperature8, (float)temperature8/2);
        }
#if 0
        else /* error */
        {
          pValueParams->data[0] = 0xFF;
        }
#endif
      }
      else /* Value not to be read from sensor */
      {
        pValueParams->data[0] = PresentTemperatureValue;
      }
    }
    else if (sensorOffset == 1) /* Pressure */
    {
//      tempStatus = LPS25HB_GetPressure(&pressure);
//      if (tempStatus == PRESSURE_OK)
      {
        TRACE_M(TF_SENSOR, "Pressure sensor value %f mbar\r\n" , pressure);
      
        memcpy(pValueParams->data, (void*)&pressure, 4);
      }
#if 0
      else /* error */
      {
        memset(pValueParams->data, 0, 4);
      }
#endif
    }
  }
  else
  {
    result = MOBLE_RESULT_FAIL;
  }
  
  return result;
}
  
#endif
  

/**
  * @brief  Callback corresponding to Appli_Sensor_Cadence_Status
  * @param  Descriptor parameters
  * @param  
* @retval None
*/ 
void Appli_Sensor_Cadence_Status(const MOBLEUINT8 *pCadence,
                                 MOBLEUINT32 length,
                                 MOBLE_ADDRESS dstPeer,
                                 MOBLEUINT8 elementIndex)
{
  MOBLEUINT8 i;
  
  TRACE_M(TF_SENSOR,"Appli_Sensor_Cadence_Status callback received \r\n");
  
  TRACE_M(TF_SERIAL_CTRL,"#%d! for element %d \r\n", 
          SENSOR_CADENCE_STATUS,
          elementIndex);
  for(i = 0; i < length; i++)
  {
    TRACE_M(TF_SERIAL_CTRL,"Cadence value: %d\n\r", pCadence[i]);
  }
}
      
      
/**
  * @brief  Callback corresponding to Appli_Sensor_Settings_Status
  * @param  Descriptor parameters
  * @param  
* @retval None
*/ 
void Appli_Sensor_Settings_Status(const MOBLEUINT8 *pSettings,
                                  MOBLEUINT32 length,
                                  MOBLE_ADDRESS dstPeer,
                                  MOBLEUINT8 elementIndex)
{
  MOBLEUINT8 i;
  
  TRACE_M(TF_SENSOR,"Appli_Sensor_Settings_Status callback received \r\n");
  
  TRACE_M(TF_SERIAL_CTRL,"#%d! for element %d \r\n", 
          SENSOR_SETTINGS_STATUS,
          elementIndex);
  for(i = 0; i < length; i++)
  {
    TRACE_M(TF_SERIAL_CTRL,"Settings value: %d\n\r", pSettings[i]);
  }
}
      
      
/**
  * @brief  Callback corresponding to Appli_Sensor_Setting_Status
  * @param  Descriptor parameters
  * @param  
* @retval None
*/ 
void Appli_Sensor_Setting_Status(const MOBLEUINT8 *pSetting,
                                  MOBLEUINT32 length,
                                  MOBLE_ADDRESS dstPeer,
                                  MOBLEUINT8 elementIndex)
{
  MOBLEUINT8 i;
  
  TRACE_M(TF_SENSOR,"Appli_Sensor_Setting_Status callback received \r\n");
  
  TRACE_M(TF_SERIAL_CTRL,"#%d! for element %d \r\n", 
          SENSOR_SETTING_STATUS,
          elementIndex);
  for(i = 0; i < length; i++)
  {
    TRACE_M(TF_SERIAL_CTRL,"Setting value: %d\n\r", pSetting[i]);
  }
}
      
      
/**
  * @brief  Callback corresponding to Appli_Sensor_Descriptor_Status
  * @param  Descriptor parameters
  * @param  
* @retval None
*/ 
void Appli_Sensor_Descriptor_Status(const MOBLEUINT8 *pDescriptor,
                                    MOBLEUINT32 length,
                                    MOBLE_ADDRESS dstPeer,
                                    MOBLEUINT8 elementIndex)
{
  MOBLEUINT8 i;
  
  TRACE_M(TF_SENSOR,"Appli_Sensor_Descriptor_Status callback received \r\n");
  
  TRACE_M(TF_SERIAL_CTRL,"#%d! for element %d \r\n", 
          SENSOR_DESCRIPTOR_STATUS,
          elementIndex);
  for(i = 0; i < length; i++)
  {
    TRACE_M(TF_SERIAL_CTRL,"Descriptor value: %d\n\r", pDescriptor[i]);
  }
}
      
      
/**
  * @brief  Callback corresponding to Appli_Sensor_Status
  * @param  Descriptor parameters
  * @param  
* @retval None
*/ 
/* MshMDLv1.0.1 - 4.2.14 Sensor status */
void Appli_Sensor_Status(const MOBLEUINT8 *pStatus,
                         MOBLEUINT32 length,
                         MOBLE_ADDRESS dstPeer,
                         MOBLEUINT8 elementIndex)
{
  MOBLEUINT16 u16PeerAddr;
  MOBLEUINT16 u16PropertyID;
  MOBLEUINT8 u8RawDataLength = 0;
  MOBLEUINT16 u16RawData = 0;
  MOBLEUINT8 u8TTL;

  MOBLEUINT32 u32RawData = 0;
  float tStart, tEnd, tDelta;

  if(g_sSensorStatusData.u8WriteFlag)
  {
	  g_sSensorStatusData.u8WriteFlag = 0;
	  g_sSensorStatusData.u8ReadFlag = 1;
	  u16PeerAddr = g_sSensorStatusData.u16PeerAddress;
	  u8TTL = g_sSensorStatusData.u8TTL;

	  u16PropertyID = ExtractPropertyID(pStatus, &u8RawDataLength);
  }

//  TRACE_M(TF_SENSOR,"Appli_Sensor_Status callback received \r\n");

  switch(u8RawDataLength)
  {
  case 1:
	  u16RawData = (MOBLEUINT16)pStatus[2];
	  break;
  case 2:
	  u16RawData = ((MOBLEUINT16)pStatus[2] << 8);
	  u16RawData |= (MOBLEUINT16)pStatus[3];
	  break;
  case 3:
	  u32RawData  = ((MOBLEUINT32)pStatus[2] << 16);
	  u32RawData |= (MOBLEUINT32)pStatus[3] << 8;
	  u32RawData |= (MOBLEUINT32)pStatus[4];
	  g_sMsgTimePropagation.ui16CounterRxMsg++;
	  g_sMsgTimePropagation.ui16CounterTxMsg = (uint16_t)(pStatus[5] << 8) | (uint16_t)(pStatus[6]);	/* Total send msg by sensor server */
	  break;
  default:
	  ;
  }

  if(g_sSensorData[u16PeerAddr].bPrintMeasValueEnable == 1)
  {
	  switch(u16PropertyID)
	  {
	  case PRESENT_AMBIENT_TEMPERATURE_PID:
		  TRACE_I(TF_APPLI_SENSOR_STATUS_CUSTOM, "peer_add = 0x%04x: Present ambient temperature: %u [degC]\r\n", u16PeerAddr, u16RawData);
	    break;
	  case MOTION_SENSED_PID:
		  TRACE_I(TF_APPLI_SENSOR_STATUS_CUSTOM, "peer_add = 0x%04x: Motion sensed, raw value: %u\r\n", u16PeerAddr, u16RawData);
	    break;
	  case PRESENT_AMBIENT_CO2_CONCENTRATION_PID:
		  TRACE_I(TF_APPLI_SENSOR_STATUS_CUSTOM, "peer_add = 0x%04x: CO2: %u [ppm]\r\n", u16PeerAddr, u16RawData);
	    break;
	  case TOTAL_DEVICE_STARTS_PID:
		  /* This characterictic was used only for time measurement */
		  g_sMsgTimePropagation.ui32CaptureStart = u32RawData;
		  tStart = (float)g_sMsgTimePropagation.ui32CaptureStart;		/* Time when remote node send Sensor status */
		  tStart = tStart / 10;											/* Convert to [ms] */
		  tEnd =  (float)g_sMsgTimePropagation.ui32CaptureEnd;			/* Time when concentrator receive Sensor status from remote node */
		  tEnd = tEnd / 10;												/* Convert to [ms] */
		  tDelta = tEnd - tStart;										/* Message propagation time */
#if (MATLAB_PRINT == 0)
		  TRACE_I(TF_APPLI_SENSOR_STATUS_CUSTOM, \
				  "peer = 0x%04x, dst = 0x%04x, length %lu, tSta: %.1f ms, tEnd: %.1f ms, tDelta: %.1f ms, ttl = %d, Tx: %d, Rx: %d\r\n",\
				  u16PeerAddr, dstPeer, length, tStart, tEnd, tDelta, u8TTL,\
				  g_sMsgTimePropagation.ui16CounterTxMsg, g_sMsgTimePropagation.ui16CounterRxMsg);
#else
		  TRACE_I(TF_APPLI_SENSOR_STATUS_CUSTOM, "%lu; %.1f; %d; %d; %d\r\n",\
				  length, tDelta, u8TTL, g_sMsgTimePropagation.ui16CounterTxMsg, g_sMsgTimePropagation.ui16CounterRxMsg);
#endif
	    break;
	  default:
		  TRACE_I(TF_APPLI_SENSOR_STATUS_CUSTOM, "peer_add = 0x%04x: Unknown property ID: 0x%04x, raw value: %u\r\n",u16PeerAddr, u16PropertyID, u16RawData);
		  break;
	  }
  }


  /* Save measured data to g_sSensorData. When log is enabled, accumulated data are cleared in LogEnableRequet() . */
  g_sSensorData[u16PeerAddr].ui32AccMeas += (uint32_t)u16RawData;	/* Accumulate measured concentration */
  g_sSensorData[u16PeerAddr].ui8CounterMeas++;
  g_sSensorData[u16PeerAddr].ui16PropertyID = u16PropertyID;
  
}
      
      
/**
  * @brief  Callback corresponding to Appli_Sensor_Column_Status
  * @param  Descriptor parameters
  * @param  
* @retval None
*/ 
void Appli_Sensor_Column_Status(const MOBLEUINT8 *pColumn,
                                MOBLEUINT32 length,
                                MOBLE_ADDRESS dstPeer,
                                MOBLEUINT8 elementIndex)
{
  MOBLEUINT8 i;
  
  TRACE_M(TF_SENSOR,"Appli_Sensor_Column_Status callback received \r\n");
  
  TRACE_M(TF_SERIAL_CTRL,"#%d! for element %d \r\n", 
          SENSOR_COLUMN_STATUS,
          elementIndex);
  for(i = 0; i < length; i++)
  {
    TRACE_M(TF_SERIAL_CTRL,"Column Status value: %d\n\r", pColumn[i]);
  }
}
      
      
/**
  * @brief  Callback corresponding to Appli_Sensor_Series_Status
  * @param  Descriptor parameters
  * @param  
* @retval None
*/ 
void Appli_Sensor_Series_Status(const MOBLEUINT8 *pSeries,
                                MOBLEUINT32 length,
                                MOBLE_ADDRESS dstPeer,
                                MOBLEUINT8 elementIndex)
{
  MOBLEUINT8 i;
  
  TRACE_M(TF_SENSOR,"Appli_Sensor_Series_Status callback received \r\n");
  
  TRACE_M(TF_SERIAL_CTRL,"#%d! for element %d \r\n", 
          SENSOR_SERIES_STATUS,
          elementIndex);
  for(i = 0; i < length; i++)
  {
    TRACE_M(TF_SERIAL_CTRL,"Series Status value: %d\n\r", pSeries[i]);
  }
}
      
      
/**
  * @brief  Callback corresponding to Sensor_ReadColumn_cb
  *         Fill column width and raw valye Y in data buffer
  *         data length <= 8
  * @param  Sensor offset in sensor init structure
  * @param  Series column parameters
  * @retval Fail if sensor doesn't exist
  *         else Success
*/  
MOBLE_RESULT Appli_Sensor_ReadColumn(MOBLEUINT8 sensorOffset,
                                     MOBLEUINT8 columnOffset,
                                     sensor_ColumnCbParams_t* pColumnParams)
{
  MOBLE_RESULT result = MOBLE_RESULT_SUCCESS;
  const sensor_init_params_t* pSensorInitParams = NULL;
  MOBLEUINT8 dataLength = 0;
  MOBLEUINT8* data = pColumnParams->data;
  
  if (sensorOffset < SensorServerInitParams.sensorsCount)
  {
    pSensorInitParams = &(SensorServerInitParams.sensorInitParams[sensorOffset]);
    
    /* fill sensor column data */
    memcpy(data+dataLength, 
           &(pSensorInitParams->seriesColumn[columnOffset].columnWidth), 
           pSensorInitParams->dataLength);
    dataLength = pSensorInitParams->dataLength;
    
    memset(data+dataLength, 0xAA, pSensorInitParams->dataLength);
    dataLength += pSensorInitParams->dataLength;
    
    pColumnParams->dataLength = dataLength;
  }
  else
  {
    result = MOBLE_RESULT_FAIL;
  }
  
  return result;
}


/**
  * @brief  Callback corresponding to Sensor_ReadSeries_cb
  *         Fill sensor series state for all columns between and including X1 and X2
  *         Series data to be concatenated in triplet of raw value X, column width and raw value Y
  *         X[n] CW[n] Y[n] X[n+1] CW[n+1] Y[n+1] ...
  *         data length should be less than minimum of 379 or max application packet length supported
  * @param  Sensor offset in sensor init structure
  * @param  Series parameters
  * @retval Fail if sensor doesn't exist
  *         else Success
  */
MOBLE_RESULT Appli_Sensor_ReadSeries(MOBLEUINT8 sensorOffset,
                                     sensor_SeriesCbParams_t* pSeriesParams)
{
  MOBLE_RESULT result = MOBLE_RESULT_SUCCESS;
  const sensor_init_params_t* pSensorInitParams = NULL;
  MOBLEUINT16 dataLength = 0;
  MOBLEUINT8* data = pSeriesParams->data;
  
  if (sensorOffset < SensorServerInitParams.sensorsCount)
  {
    pSensorInitParams = &(SensorServerInitParams.sensorInitParams[sensorOffset]);
    
    for (MOBLEUINT8 count=0; count<pSensorInitParams->seriesCount; count++)
    {
      /* fill sensor series data */
      if (pSensorInitParams->seriesColumn[count].rawX >= pSeriesParams->rawValueX1 &&
          pSensorInitParams->seriesColumn[count].rawX <= pSeriesParams->rawValueX2)
      {
        memcpy(data+dataLength, 
               &(pSensorInitParams->seriesColumn[count].rawX), 
               pSensorInitParams->dataLength);
        dataLength += pSensorInitParams->dataLength;
        memcpy(data+dataLength, 
               &(pSensorInitParams->seriesColumn[count].columnWidth), 
               pSensorInitParams->dataLength);
        dataLength += pSensorInitParams->dataLength;
        memset(data+dataLength, 0xAA, pSensorInitParams->dataLength);
        dataLength += pSensorInitParams->dataLength;
        
        pSeriesParams->dataLength = dataLength;
      }
      else
      {
        /* */
      }
    }
  }
  else
  {
    result = MOBLE_RESULT_FAIL;
  }
  
  return result;
}
  
  
/**
  * @brief  Callback corresponding to Sensor_IsFastCadence_cb
  *         To check if fast cadence to be used for current sensor state
  * @param  Sensor offset in sensor init structure
  * @param  Fast Cadence Low
  * @param  Fast Cadence High
  * @retval Trigger status
  */
MOBLEUINT8 Appli_Sensor_IsFastCadence(MOBLEUINT8 sensorOffset,
                                      void* pFastCadenceLow, 
                                      void* pFastCadenceHigh)
{
  MOBLEUINT8 fastCadenceStatus = 0;
  MOBLEUINT32 fastCadenceLow = *((MOBLEUINT32*)pFastCadenceLow);
  MOBLEUINT32 fastCadenceHigh = *((MOBLEUINT32*)pFastCadenceHigh);
  MOBLEUINT32 sensorValue = PresentTemperatureValue;
   
  if (sensorOffset == 0)
  {
    if (fastCadenceLow <= fastCadenceHigh)
  {
      fastCadenceStatus = sensorValue >= fastCadenceLow &&
                          sensorValue <= fastCadenceHigh;
    }
    else
    {
      fastCadenceStatus = sensorValue > fastCadenceLow ||
                          sensorValue < fastCadenceHigh;
  }
}
  else
  {
    /* Implmented only for sensor at offset 0 */
  }
  
  return fastCadenceStatus;
}


/**
  * @brief  Callback corresponding to Sensor_IsStatusTrigger_cb
  *         To check if sensor change in sensor state (delta) is more than
  *         given trigger state value 
  *         delta up values
  * @param  Sensor offset in sensor init structure
  * @param  Trigger type -> value or percent change
  * @param  Status trigger delta down
  * @param  Status trigger delta up
  * @retval Trigger status
*/ 
MOBLEUINT8 Appli_Sensor_IsStatusTrigger(MOBLEUINT8 sensorOffset,
                                        status_trigger_type_e triggerType,
                                        void* pDeltaDown,
                                        void* pDeltaUp)
{
  const sensor_init_params_t* pSensorInitParams = NULL;
  MOBLEUINT8 triggerStatus = 0;
  MOBLEUINT32 deltaDown = *((MOBLEUINT32*)pDeltaDown);
  MOBLEUINT32 deltaUp = *((MOBLEUINT32*)pDeltaUp);
  status_trigger_delta_e statusTriggerDelta;
  MOBLEUINT32 delta;
  
  if (sensorOffset == 0)
  {
    if (PresentTemperatureValue < PreviousTemperatureValue)
    {
      /* status trigger delta down */
      statusTriggerDelta = STATUS_TRIGGER_DELTA_DOWN;
      delta = PreviousTemperatureValue - PresentTemperatureValue;

      TRACE_M(TF_SENSOR, "Delta down value %ld\r\n", delta);
    }
    else
    {
      /* status trigger delta up */
      statusTriggerDelta = STATUS_TRIGGER_DELTA_UP;
      delta = PresentTemperatureValue - PreviousTemperatureValue;

      TRACE_M(TF_SENSOR, "Delta up value %ld\r\n", delta);
    }
  
    if (triggerType == STATUS_TRIGGER_TYPE_PC)
    {
      pSensorInitParams = &(SensorServerInitParams.sensorInitParams[sensorOffset]);
    
      if (pSensorInitParams->valuesRange != 0)
      {
        /* change delta to percentage change (of 0.01 % steps) */
        delta = (MOBLEUINT32)((delta*10000)/pSensorInitParams->valuesRange);
      }
      else
      {
        triggerStatus = 0;
      }
    }
  
    if ((statusTriggerDelta == STATUS_TRIGGER_DELTA_DOWN && delta >= deltaDown) ||
        (statusTriggerDelta == STATUS_TRIGGER_DELTA_UP && delta >= deltaUp))
    {
      triggerStatus = 1;
    }
    else
    {
      triggerStatus = 0;
    }
  }

  return triggerStatus;
}
  
  
/**
* @brief  Initialize hardware interfaces for sensors and mesh sensor model structures
*         Sensor init parameters to be defined in sensor_cfg_usr.h
*         Sensors are initialized in the order as defined in sensor_cfg_usr.h
  *         LPS25HB supports pressure and temperature sensor
* @param  void 
  * @retval 
  */
MOBLE_RESULT Appli_Sensor_Init(void)                                        
  {
  MOBLE_RESULT result = MOBLE_RESULT_SUCCESS;
//  PRESSURE_StatusTypeDef lps25Status;
    
  /* Hardware interface initialization */
#ifndef CUSTOM_BOARD_PWM_SELECTION  
//  PRESSURE_StatusTypeDef tempStatus;
//  PRESSURE_StatusTypeDef pressStatus;
//  int16_t temperature;
//  int32_t pressure;
  
#if 0
  /* Initiallization of sensors */
  lps25Status = LPS25HB_Init((PRESSURE_InitTypeDef*)(&Lps25InitParams));
        
  if (lps25Status != PRESSURE_OK)
  {
    TRACE_M(TF_SENSOR, "Error initializing LPS25HB. status (%d)\r\n", lps25Status);
  }
    
  tempStatus = LPS25HB_I2C_ReadRawTemperature(&temperature);
  pressStatus = LPS25HB_I2C_ReadRawPressure(&pressure);

  if (tempStatus == PRESSURE_OK &&
      pressStatus == PRESSURE_OK)
  {
    /* TRACE_M(TF_SENSOR, "Raw temperature (%d) and raw pressure (%d)\r\n",
                        temperature, pressure); */
  }
  else
  {
    TRACE_M(TF_SENSOR, "Error reading LPS25HB at init\r\n");
  }
#endif
  
#else
  TRACE_M(TF_SENSOR, "LPS25HB not initialized\r\n");
#endif

  /* initialize sensor server model */
  result = SensorServer_Init(&BufferSensorServer, 
                              &AppliSensor_cb,
                              TOTAL_SENSOR_SERVER_BUFF_SIZE, 
                              &SensorServerInitParams);
      
  if(MOBLE_FAILED(result))
  {
    TRACE_M(TF_SENSOR, "Sensor Server init failed\r\n");
  }
  
  return result; 
  }


/**
  * @brief  Handling of serial inputs to sensor model
  * @param  Sensor offset in sensor init structure
  * @param  Sensor value
  * @retval void
  */
MOBLE_RESULT Appli_Sensor_Update(MOBLEUINT8 sensorOffset, MOBLEUINT32 value)
{
  MOBLE_RESULT result = MOBLE_RESULT_SUCCESS;
  
  AppliSensorReadFromSensor = 1;
  
  /* Update previous with current and current with new */
  PreviousTemperatureValue = PresentTemperatureValue;
  PresentTemperatureValue = (MOBLEUINT8)value;
  
  /* To update cadence parameters
     results in either status trigger or (and) fast cadence based on change and present
     Application may decide not to update cadence parameters if senosr value is changing
     very frequently in some cases to save bandwidth */
  if (PresentTemperatureValue != PreviousTemperatureValue)
  {
    result = Sensor_UpdateCadence(sensorOffset, 
                  SensorServerInitParams.sensorInitParams[0].elementIdx, 
                                  SensorServerInitParams.sensorInitParams[0].propertyId);
  }
  
  return result;
}


/**
  * @brief  Handling of serial inputs to sensor model
  *         Appli_Sensor_SerialCmd can be used for testing periodic publishing and 
  *         triggered publishing with PTS
  * @param  serial string
  * @param  serial string size
  * @retval void
  */
void Appli_Sensor_SerialCmd(char *rcvdStringBuff, uint16_t rcvdStringSize)
{
  MOBLE_RESULT result = MOBLE_RESULT_INVALIDARG;
  MOBLEUINT16 value = 0;
  MOBLEUINT8 sensorOffset = 0;
  
  if (!strncmp(rcvdStringBuff+6, "SETV", 4))
  {
    if (rcvdStringSize == 15)
    {
      sscanf(rcvdStringBuff+11, "%4hx", &value);
      
      /* Set SENSOR1 value at offset 0 */
      result = Appli_Sensor_Update(sensorOffset, value);
    }
    else
    {
      result = MOBLE_RESULT_FAIL;
      BLEMesh_PrintStringCb("Invalid size of string\r\n");
    }
  }
  else if (!strncmp(rcvdStringBuff+6, "PUBLISH", 7))
  {
    if (rcvdStringSize == 17)
    {
      sscanf(rcvdStringBuff+14, "%1hx", &value);
      
      sensorOffset = value;
      
      sscanf(rcvdStringBuff+16, "%1hx", &value);
      
      /* Enable / Disable publishing of sensor as identified by sensor offset */
      if(value == 0) /* Disable */
      {
        result = Sensor_UpdatePublishState(sensorOffset, 0);
      }
      else /* Enable */
      {
        result = Sensor_UpdatePublishState(sensorOffset, 1);
      }
    }
    else
    {
      result = MOBLE_RESULT_FAIL;
      BLEMesh_PrintStringCb("Invalid size of string\r\n");
    }
  }
  else
  {
    result = MOBLE_RESULT_FAIL;
  } 
  
  /* Check the result of command processing */
  if(result == MOBLE_RESULT_SUCCESS)
  {
    BLEMesh_PrintStringCb("Success\r\n");
  }
  else if(result == MOBLE_RESULT_OUTOFMEMORY)
  {
    BLEMesh_PrintStringCb("Fail Out of memory\r\n");  
  }
  else if(result == MOBLE_RESULT_INVALIDARG)
  {
    BLEMesh_PrintStringCb("Fail Invalid Argument\r\n");  
  }
  else
  {       
    BLEMesh_PrintStringCb("Fail\r\n");   
  }
}

/* Extract property ID and lenght ... MshMDLv1.0.1 4.2.14 */
static uint16_t ExtractPropertyID(const uint8_t *pStatus, uint8_t *pLength)
{
	uint16_t u16PropertyID;

	if(*pStatus & 0x01)
	{
	  /* format B */
	  *pLength = (((*pStatus >> 1) & 0x7f) + 1) & 0x7f;
	  u16PropertyID = (MOBLEUINT16)(((MOBLEUINT16)(*(pStatus+1))) | ((MOBLEUINT16)(*(pStatus+1)) << 8));
	}
	else
	{
	  /* format A */
	  *pLength = (((*pStatus >> 1) & 0x0f) + 1) & 0x0f;
	  u16PropertyID = (MOBLEUINT16)((*(pStatus+1)) << 3) | ((*pStatus >> 5) & 0x07);
	}

	return u16PropertyID;
}


/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2020 STMicroelectronics *****END OF FILE****/

